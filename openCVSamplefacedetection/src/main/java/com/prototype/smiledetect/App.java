package com.prototype.smiledetect;

import android.app.Application;
import android.graphics.Point;
import android.util.Log;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Антон on 04.06.2016.
 */
public class App extends Application {

    public static final String TAG = App.class.getSimpleName();

    public static int sScreenWidth;
    public static int sScreenHeight;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        obtainScreenDimensions();
    }

    public void obtainScreenDimensions() {
        Point p = new Point();
        ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay().getSize(p);
        sScreenWidth = p.x;
        sScreenHeight = p.y;
        Log.d(TAG, "screenWidth = " + sScreenWidth + ", screenHeight = " + sScreenHeight);
    }
}
