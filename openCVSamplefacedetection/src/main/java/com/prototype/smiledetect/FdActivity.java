package com.prototype.smiledetect;

import java.io.File;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;


import com.prototype.smiledetect.custom.CameraBridgeViewBase;

import util.Util;

public class FdActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final String TAG = "OCVSample::Activity";
    private static final Scalar FACE_RECT_COLOR = new Scalar(0, 255, 0, 255);
    private static final Scalar SMILE_RECT_COLOR = new Scalar(0, 0, 255, 120);
    private static final Scalar MOUTH_RECT_COLOR = new Scalar(255, 0, 0, 250);
    public static final int JAVA_DETECTOR = 0;
    public static final int NATIVE_DETECTOR = 1;

    private MenuItem mItemFace50;
    private MenuItem mItemFace40;
    private MenuItem mItemFace30;
    private MenuItem mItemFace20;
    private MenuItem mItemType;

    private Mat mRgba;
    private Mat mGray;
    private File mCascadeFileFace;
    private File mCascadeFileSmile;
    private File mCascadeFileMouth;
    //  private CascadeClassifier mJavaDetectorFace;
    //  private CascadeClassifier mJavaDetectorSmile;
    private DetectionBasedTracker mNativeDetectorFace;
    private DetectionBasedTracker mNativeDetectorSmile;
    private DetectionBasedTracker mNativeDetectorMouth;

    private int mDetectorType = NATIVE_DETECTOR;
    private String[] mDetectorName;

    private float mRelativeFaceSize = 0.2f;
    private int mAbsoluteFaceSize = 0;

    private float mRelativeSmileSize = 0.15f;
    private volatile int mAbsoluteMouthSize = 0;

    private CameraBridgeViewBase mOpenCvCameraView;
    private ImageView mFaceTestImageView;
    private SeekBar mMouthMinSizeBar;
    private TextView mMouthMinSizeText;

    static {
        System.loadLibrary("opencv_java3");
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("detection_based_tracker");

                    mCascadeFileFace = Util.makeCascadeFile(FdActivity.this, R.raw.haarcascade_frontalface_alt, "haarcascade_frontalface_alt.xml");
                    mCascadeFileSmile = Util.makeCascadeFile(FdActivity.this, R.raw.smile_cascade_2, "smile_cascade_2.xml");
                    mCascadeFileMouth = Util.makeCascadeFile(FdActivity.this, R.raw.haarascade_mouth, "haarcascade_mouth.xml");
                    initNativeDetectors();

                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


 /*   private void initJavaDetectors() {
        mJavaDetectorFace = new CascadeClassifier(mCascadeFileFace.getAbsolutePath());
        if (mJavaDetectorFace.empty()) {
            Log.e(TAG, "Failed to load face cascade classifier");
            mJavaDetectorFace = null;
        } else {
            Log.i(TAG, "Loaded face cascade classifier from " + mCascadeFileFace.getAbsolutePath());
        }

        mJavaDetectorSmile = new CascadeClassifier(mCascadeFileSmile.getAbsolutePath());
        if(mJavaDetectorSmile.empty()) {
            Log.e(TAG, "Failed to load smile cascade classifier");
            mJavaDetectorSmile = null;
        } else {
            Log.d(TAG, "Loaded smile cascade classifier from " + mCascadeFileSmile.getAbsolutePath());
        }
    }*/

    private void initNativeDetectors() {
        mNativeDetectorFace = new DetectionBasedTracker(mCascadeFileFace.getAbsolutePath(), 0);
        mNativeDetectorSmile = new DetectionBasedTracker(mCascadeFileSmile.getAbsolutePath(), 0);
        mNativeDetectorMouth = new DetectionBasedTracker(mCascadeFileMouth.getAbsolutePath(), 0);
        mNativeDetectorFace.start();
        mNativeDetectorSmile.start();
        mNativeDetectorMouth.start();
    }

    public FdActivity() {
        mDetectorName = new String[2];
        mDetectorName[JAVA_DETECTOR] = "Java";
        mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";

        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.face_detect_surface_view);
        mFaceTestImageView = (ImageView) findViewById(R.id.fd_activity_face_imv);

        mMouthMinSizeText = (TextView) findViewById(R.id.fd_activity_progress_text);
        mMouthMinSizeBar = (SeekBar) findViewById(R.id.fd_activity_progress_minsize);
        mMouthMinSizeBar.setMax(50);
        mMouthMinSizeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mAbsoluteMouthSize = progress;
                mMouthMinSizeText.setText("" + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        mMouthMinSizeBar.setVisibility(View.GONE);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setVisibility(CameraBridgeViewBase.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        //      mOpenCvCameraView.setCameraIndex(1);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        // if (!OpenCVLoader.initDebug()) {
        //      Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
        //      OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
        //  } else {
        Log.d(TAG, "OpenCV library found inside package. Using it!");
        mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        //  }
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }


    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Log.d(TAG, "handle frame");
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        Mat outputFrame = mRgba;


        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
            mNativeDetectorFace.setMinFaceSize(mAbsoluteFaceSize);
        }
        if (mNativeDetectorFace != null && mNativeDetectorMouth != null && mNativeDetectorSmile != null) {
            Rect[] facesArray = detect(mNativeDetectorFace, mGray);

            Log.d(TAG, "detected " + facesArray.length + " faces");
            for (int i = 0; i < facesArray.length; i++) {
                //           Imgproc.rectangle(mGray, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);

                Point tl = facesArray[i].tl();

                Rect roi = new Rect((int) facesArray[i].tl().x,
                        (int) (facesArray[i].tl().y),
                        facesArray[i].width,
                        facesArray[i].height);
                Imgproc.rectangle(outputFrame, tl, facesArray[i].br(), FACE_RECT_COLOR, 2);
                try {
                    if (roi.x >= 0 && roi.width >= 0 && roi.y >= 0 && roi.height >= 0) {

                        final Mat smileRoi = mGray.submat(roi); //correct face
                     /*   runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Bitmap bmp = Bitmap.createBitmap(smileRoi.width(), smileRoi.height(), Bitmap.Config.RGB_565);
                                Utils.matToBitmap(smileRoi, bmp);
                                mFaceTestImageView.setImageBitmap(bmp);
                            }
                        });*/

                        //set suitable sizes
                        int height = smileRoi.rows();
                        if (Math.round(height * mRelativeSmileSize) > 0) {
                            mAbsoluteMouthSize = Math.round(height * mRelativeSmileSize);
                        }
                        mNativeDetectorSmile.setMinFaceSize(mAbsoluteMouthSize);
                        mNativeDetectorMouth.setMinFaceSize(mAbsoluteMouthSize);


                        Rect[] smilesArray = detect(mNativeDetectorSmile, smileRoi);
                        Log.d(TAG, "detected " + smilesArray.length + " smiles");
                        //    for (int j = 0; j < smilesArray.length; j++) {
               //         Rect[] mouthArray = detect(mNativeDetectorMouth, smileRoi);
           //             Log.d(TAG, "detected " + mouthArray.length + " mouths");

                  /*      if (mouthArray.length > 0) {
                            for (int j = 0; j < mouthArray.length; j++) {
                                Point tl2 = mouthArray[j].tl();
                                Point br2 = mouthArray[j].br();

                                tl2.set(new double[]{tl.x + tl2.x, tl.y + tl2.y});
                                br2.set(new double[]{tl.x + br2.x, tl.y + br2.y});

                                Imgproc.rectangle(outputFrame, tl2, br2, SMILE_RECT_COLOR, 2);
                            }
                        }*/
                        Rect smile = getMostSuitableSmile(smilesArray, smileRoi.height(), smileRoi.width());
                        if (smile != null) {
                            Point tl2 = smile.tl();
                            Point br2 = smile.br();

                            tl2.set(new double[]{tl.x + tl2.x, tl.y + tl2.y});
                            br2.set(new double[]{tl.x + br2.x, tl.y + br2.y});

                            Imgproc.rectangle(outputFrame, tl2, br2, MOUTH_RECT_COLOR, 2);
                        }


                        //     }

                    }
                } catch (CvException e) {
                    e.printStackTrace();
                }
            }
        }

        return outputFrame;
    }

    static final int FACE_MOUTH_OFFSET = 30;

    private Rect getMostSuitableSmile(Rect[] smileArray, int faceHeight, int faceWidth) {
        if (smileArray == null || smileArray.length == 0) {
            return null;
        }
        for (Rect smile : smileArray) {
            float smileTop = (float) smile.tl().y;
            float smileMiddleX = (float) ((smile.tl().x + smile.br().x) / 2);

            float faceMouthLeftBound = ((float) faceWidth / 2) - (float) (FACE_MOUTH_OFFSET * faceWidth) / 100;
            float faceMouthRightBound = ((float) faceWidth / 2) + (float) (FACE_MOUTH_OFFSET * faceWidth) / 100;
            if (smileTop > ((float) faceHeight / 2)
                    && (smileMiddleX >= faceMouthLeftBound && smileMiddleX <= faceMouthRightBound)) {
                return smile;
            }
        }
        return null;

    }

    private Rect[] detect(DetectionBasedTracker detector, Mat roi) {
        MatOfRect objects = new MatOfRect();
        if (detector != null) {
            if (detector == mNativeDetectorSmile) {
                detector.setMinFaceSize(mAbsoluteMouthSize);
            }
            detector.detect(roi, objects);
        }
        return objects.toArray();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        mItemFace50 = menu.add("Face size 50%");
        mItemFace40 = menu.add("Face size 40%");
        mItemFace30 = menu.add("Face size 30%");
        mItemFace20 = menu.add("Face size 20%");
        mItemType = menu.add(mDetectorName[mDetectorType]);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item == mItemFace50)
            setMinFaceSize(0.5f);
        else if (item == mItemFace40)
            setMinFaceSize(0.4f);
        else if (item == mItemFace30)
            setMinFaceSize(0.3f);
        else if (item == mItemFace20)
            setMinFaceSize(0.2f);
        else if (item == mItemType) {
            //   int tmpDetectorType = (mDetectorType + 1) % mDetectorName.length;
            //   item.setTitle(mDetectorName[tmpDetectorType]);
            //   setDetectorType(tmpDetectorType);
        }
        return true;
    }

    private void setMinFaceSize(float faceSize) {
        mRelativeFaceSize = faceSize;
        mAbsoluteFaceSize = 0;
    }

    private void setDetectorType(int type) {
        if (mDetectorType != type) {
            mDetectorType = type;

            if (type == NATIVE_DETECTOR) {
                Log.i(TAG, "Detection Based Tracker enabled");
                mNativeDetectorFace.start();
                mNativeDetectorSmile.start();
            } else {
                Log.i(TAG, "Cascade detector enabled");
                //     mNativeDetector.stop();
            }
        }
    }
}
